<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Http\Request;
use App\Conversations\ExampleConversation;
use Spatie\SslCertificate\Exceptions\CouldNotDownloadCertificate;
use Spatie\SslCertificate\Exceptions\InvalidUrl;
use Spatie\SslCertificate\SslCertificate;
use Exception;

class BotManController extends Controller
{
    private $user;
    private $users;
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');
        $botman->hears('ssl-info {url}', function ($bot, $url) {
            \Log::alert('url', [$url]);
            try {
                $certificate = SslCertificate::createForHostName($url);
            }
            catch (InvalidUrl $e){
                $certificate = null;
            }
            catch (CouldNotDownloadCertificate $e) {
                $certificate = null;
            }

            if($certificate) {
                $issuer = $certificate->getIssuer();
                $date = $certificate->expirationDate()->diffInDays();
                $valid = ($certificate->isValid() == 1) ? 'True' : 'False';
                $bot->reply('Issuer: ' . $issuer);
                $bot->reply('Is Valid: ' . $valid);
                $bot->reply('Expired In: ' . $date . ' days');
            }
            else {
                $bot->reply('Error! Check domain again.');
            }

        });

        $botman->hears('subscribe', function ($bot) {
            $this->user = $bot->getUser();
            $bot->userStorage()->save([
                'username' => $this->user->getUsername(),
                'first_name' => $this->user->getFirstName(),
                'id' => $this->user->getId(),
                'info' => $this->user->getInfo(),
            ]);
            $bot->reply('For unsubscribe send:unsubscribe');
        });
        $botman->hears('unsubscribe', function ($bot) {
            $this->user = $bot->getUser();
            $bot->userStorage()->delete($this->user->getId());
            $bot->reply('You unsubscribe');
        });
        $botman->listen();


    }
    public function send(Request $request)
    {
        $botman = app('botman');

        foreach ($request->users as $user){
            if($user['check']) {
                $botman->say($request->message, $user['id'], TelegramDriver::class);
            }
        }
        $botman->listen();
    }
    public function delete(Request $request)
    {
        $botman = app('botman');

        foreach ($request->users as $user){
            if($user['check']) {
                $botman->userStorage()->delete($user['id']);
            }
        }
        $users = [];
        foreach ($botman->userStorage()->all() as $user) {
            $users[] = [
                'name' => $user->get('first_name'),
                'id' => $user->get('id'),
                'check' => true,
            ];
        }

        return response()->json($users);
    }
    public function index()
    {
        $botman = app('botman');

        $users = [];
        foreach ($botman->userStorage()->all() as $user) {
            $users[] = [
                'name' => $user->get('first_name'),
                'id' => $user->get('id'),
                'check' => true,
            ];
        }
        return view('welcome', [
            'users' => $users
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        return view('tinker');
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }
}
