<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BotManController@index');

Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::match(['get', 'post'], '/send', 'BotManController@send');
Route::match(['get', 'post'], '/delete', 'BotManController@delete');
//Route::get('/botman/tinker', 'BotManController@tinker');
