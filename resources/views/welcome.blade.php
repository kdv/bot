<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Telegram Bot</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" type="text/css">
    <link href="/css/app.css" rel="stylesheet" type="text/css">

    <!-- Styles -->
</head>
<body>
<div class="container" id="app">
    <div class="content">
        <div class="col-md-8 col-md-offset-2" style="margin-top: 100px;">
            <users :users="{{ json_encode($users) }}"></users>
        </div>
    </div>
</div>
</body>
<script src="/js/app.js"></script>
</html>